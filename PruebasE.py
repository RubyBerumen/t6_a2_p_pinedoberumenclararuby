'''
Created on 10 dic. 2020

@author: Ruby
'''
import time
from random import *

class BusquedaBinaria:
    def __init__(self):
        self.comparaciones = 0
        self.intercambios = 0
        self.recorridos = 0
        self.tiempo = 0
        
    def busqueda (self,array,buscado):
        inicio=0
        final=len(array)-1
        
        while inicio<= final:
            self.recorridos+=1
            puntero=(inicio+final)//2
            self.comparaciones+=1
            if buscado==array[puntero]:
                return 1
            elif buscado > array[puntero]:
                self.comparaciones+=1
                inicio=puntero+1
            else:
                final=puntero-1
        return 0
    
    def llamadaBusquedaBinaria(self,nums,valorB):
        
        self.shell(nums)
        tInicio = time.time_ns()
        res=self.busqueda(nums, valorB)
        if(res == True):
            print("Se encontro el dato")
        else:
            print("No se encontro")
        tFin = time.time_ns()
        tiempo = tFin-tInicio
        print(f"Tiempo: {tiempo}")
        print(f"Recorridos: {self.recorridos}")         
        print(f"Comparaciones: {self.comparaciones}")       
        print(f"Intercambios: {self.intercambios}\n")
            
            
    def shell(self,numeros):
        intervalo = len(numeros)/2
        intervalo = int(intervalo)
        while(intervalo>0):
            for i in range(int(intervalo),len(numeros)):
                j=i-int(intervalo)
                while(j>=0):
                    k=j+int(intervalo)
                    if(numeros[j] <= numeros[k]):
                        j-=1
                    else:
                        aux=numeros[j]
                        numeros[j]=numeros[k]
                        numeros[k]=aux
                        j-=int(intervalo)
            
            intervalo=int(intervalo)/2
        
        
class Hash:

    def __init__(self):
        self.table = [None] * 127
        self.comparaciones = 0
        self.intercambios = 0
        self.recorridos = 0
        self.tiempo = 0

    def Hash_func(self, value):
        key = 0
        for i in range(0,len(value)):
            self.recorridos+=1
            key += ord(value[i])
        return key % 127

    def Insert(self, value):
        hash = self.Hash_func(value)
        self.comparaciones+=1
        if self.table[hash] is None:
            self.table[hash] = value

    def Search(self,value): 
        hash = self.Hash_func(value)
        self.comparaciones+=1
        if self.table[hash] is None:
            return None
        else:
            return (self.table[hash])

    def Remove(self,value): 
        hash = self.Hash_func(value);
        self.comparaciones+=1
        if self.table[hash] is None:
            print("No hay elementos con ese valor", value)
        else:
            print("Elemento con valor", value, "eliminado")
            self.table[hash] is None       
            
        
    def llamadaHash(self,nums,valorB):
            
        tInicio = time.time_ns()
        if(self.Search(valorB)!=None):
            print("Se encontro el dato")
        else:
            print("No se encontro")
        print()
        tFin = time.time_ns()
        tiempo = tFin-tInicio
        print(f"Tiempo: {tiempo}")
        print(f"Recorridos: {self.recorridos}")         
        print(f"Comparaciones: {self.comparaciones}")       
        print(f"Intercambios: {self.intercambios}\n")
        

nums=[]        
for i in range(0,100):
    nums.append(randint(0,100))
    
bb=BusquedaBinaria()
h=Hash()

for i in range(0,len(nums)):
    h.Insert(str(nums[i]))
    
while True:
    print("Que metodo deseas utilizar")
    print("1. Busqueda binaria")
    print("2. Busqueda por funciones hash")
    print("3. Salir ");
    op = int (input("Escribe una opcion"))

    if(op==1):
        valorB=int(input("Ingrese el valor a buscar:"))
        print(f"Vector: {nums}")
        bb.llamadaBusquedaBinaria(nums, valorB)
    elif(op==2):
        valorB=int(input("Ingrese el valor a buscar:"))
        print(f"Vector: {nums}")
        v2=str(valorB)
        h.llamadaHash(nums, v2)
    elif(op==3):
        break
    else:
        print("Debes ingresar un numero entre 1 y 3")







        
        